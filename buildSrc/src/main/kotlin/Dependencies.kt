object Version {
	val assertj = "3.11.1"
	val junit = "5.4.0"
	val lombok = "1.18.6"
}

object Dependencies {
	val assertj = "org.assertj:assertj-core:${Version.assertj}"
	val junit = "org.junit.jupiter:junit-jupiter:${Version.junit}"
	val lombok = "org.projectlombok:lombok:${Version.lombok}"
}
