import Dependencies.assertj
import Dependencies.junit
import Dependencies.lombok
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
	java
}

repositories {
	jcenter()
}

dependencies {
	implementation(lombok)
	annotationProcessor(lombok)

	testImplementation(assertj)
	testImplementation(junit)
}

tasks.withType<Test> {
	useJUnitPlatform()
	testLogging {
		exceptionFormat = TestExceptionFormat.FULL
		showStackTraces = true
	}
}