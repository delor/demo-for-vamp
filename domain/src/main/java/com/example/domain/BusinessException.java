package com.example.domain;

import lombok.NonNull;

class BusinessException extends RuntimeException {
	public BusinessException(@NonNull String message) {
		super(message);
	}
}
