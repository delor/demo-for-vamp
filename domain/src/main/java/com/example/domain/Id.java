package com.example.domain;

abstract class Id {

	public abstract String asString();

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Id listId = (Id) o;
		return asString().equals(listId.asString());
	}

	@Override
	public int hashCode() {
		return asString().hashCode();
	}

	@Override
	public String toString() {
		return asString();
	}
}
