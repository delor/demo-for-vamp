package com.example.domain;

import lombok.NonNull;

import java.util.Objects;
import java.util.UUID;

public class ListId extends Id {
	private final UUID uuid;

	private ListId(@NonNull UUID uuid) {
		this.uuid = Objects.requireNonNull(uuid);
	}

	public static ListId generate() {
		return new ListId(UUID.randomUUID());
	}

	public static ListId of(String listId) {
		try {
			return new ListId(UUID.fromString(listId));
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("Invalid tasks list id - " + listId);
		}
	}

	@Override
	public String asString() {
		return uuid.toString();
	}
}
