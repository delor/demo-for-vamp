package com.example.domain;

import lombok.NonNull;

public class Task {
	private final TaskId id;
	private final String name;
	private boolean completed;

	public Task(
		@NonNull TaskId id,
		@NonNull String name
	) {
		this.id = id;
		this.name = name;
		this.completed = false;
	}

	public TaskId getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
}
