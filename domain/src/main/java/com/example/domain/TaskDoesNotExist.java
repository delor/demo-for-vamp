package com.example.domain;

import lombok.NonNull;

class TaskDoesNotExist extends BusinessException {
	public TaskDoesNotExist(@NonNull TaskId taskId) {
		super("Task with id=" + taskId.asString() + " does not exist");
	}
}
