package com.example.domain;

import lombok.NonNull;

import java.util.Objects;
import java.util.UUID;

public class TaskId extends Id {
	private final UUID uuid;

	private TaskId(@NonNull UUID uuid) {
		this.uuid = Objects.requireNonNull(uuid);
	}

	public static TaskId generate() {
		return new TaskId(UUID.randomUUID());
	}

	public static TaskId of(String taskId) {
		try {
			return new TaskId(UUID.fromString(taskId));
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("Invalid task id - " + taskId);
		}
	}

	@Override
	public String asString() {
		return uuid.toString();
	}
}
