package com.example.domain;

class TaskWithThisNameAlreadyExit extends BusinessException {
	public TaskWithThisNameAlreadyExit() {
		super("Task with this name already exist");
	}
}
