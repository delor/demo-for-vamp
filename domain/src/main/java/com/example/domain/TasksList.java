package com.example.domain;

import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TasksList {
	private final ListId listId;
	private final String name;
	private final List<Task> tasks;

	public TasksList(
		@NonNull ListId listId,
		@NonNull String name
	) {
		this.listId = listId;
		this.name = name;
		this.tasks = new ArrayList<>();
	}

	public TaskId createTask(@NonNull String name) {
		if (tasks.stream().anyMatch(t -> t.getName().equals(name))) {
			throw new TaskWithThisNameAlreadyExit();
		}

		TaskId taskId = TaskId.generate();
		Task task = new Task(taskId, name);
		tasks.add(task);

		return taskId;
	}

	public void changeTaskStatus(
		@NonNull TaskId taskId,
		boolean status
	) {
		Task task = tasks.stream()
			.filter(t -> t.getId().equals(taskId))
			.findAny()
			.orElseThrow(() -> new TaskDoesNotExist(taskId));

		task.setCompleted(status);
	}

	public ListId getId() {
		return listId;
	}

	public String getName() {
		return name;
	}

	public List<Task> getTasks() {
		return Collections.unmodifiableList(List.copyOf(tasks));
	}
}
