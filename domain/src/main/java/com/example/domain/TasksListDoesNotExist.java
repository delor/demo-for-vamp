package com.example.domain;

import lombok.NonNull;

class TasksListDoesNotExist extends BusinessException {
	public TasksListDoesNotExist(@NonNull ListId listId) {
		super("Tasks list with id=" + listId.asString() + " does not exist");
	}
}
