package com.example.domain;

import java.util.List;
import java.util.Optional;

public interface TasksListRepository {
	List<TasksList> findAll();

	void save(TasksList tasksList);

	Optional<TasksList> get(ListId listId);
}
