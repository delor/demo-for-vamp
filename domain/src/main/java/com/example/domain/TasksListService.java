package com.example.domain;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TasksListService {

	private final TasksListRepository repository;

	public ListId createList(
		@NonNull String name
	) {
		ListId listId = ListId.generate();
		TasksList tasksList = new TasksList(listId, name);
		repository.save(tasksList);
		return listId;
	}

	public TaskId createTask(
		@NonNull ListId listId,
		@NonNull String name
	) {
		TasksList tasksList = repository.get(listId)
			.orElseThrow(() -> new TasksListDoesNotExist(listId));
		TaskId taskId = tasksList.createTask(name);
		return taskId;
	}

	public void changeTaskStatus(
		@NonNull ListId listId,
		@NonNull TaskId taskId,
		boolean complete
	) {
		TasksList tasksList = repository.get(listId)
			.orElseThrow(() -> new TasksListDoesNotExist(listId));
		tasksList.changeTaskStatus(taskId, complete);
	}
}
