package com.example.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ListIdTest {

	@Test
	void list_id_can_be_converted() {
		ListId listId = ListId.generate();
		ListId listIdFromString = ListId.of(listId.asString());

		assertThat(listId).isEqualTo(listIdFromString);
	}
}
