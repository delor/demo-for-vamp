package com.example.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TaskIdTest {

	@Test
	void task_id_can_be_converted() {
		TaskId taskId = TaskId.generate();
		TaskId taskIdFromString = TaskId.of(taskId.asString());

		assertThat(taskId).isEqualTo(taskIdFromString);
	}
}
