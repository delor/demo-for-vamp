package com.example.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TasksListTest {

	@Test
	void cant_add_same_task_twice() {
		// given task list with one task
		TasksList tasksList = new TasksList(ListId.generate(), "list");
		tasksList.createTask("task");

		// when we try to add task with same name
		Throwable throwable = Assertions.catchThrowable(() ->
			tasksList.createTask("task"));

		// then exception is thrown
		assertThat(throwable)
			.isInstanceOf(TaskWithThisNameAlreadyExit.class);
	}

	@Test
	void cant_change_status_of_task_that_is_not_in_list() {
		// given empty list
		TasksList tasksList = new TasksList(ListId.generate(), "list");

		// when we try to change status of task
		Throwable throwable = Assertions.catchThrowable(() ->
			tasksList.changeTaskStatus(TaskId.generate(), true));

		// then exception is thrown
		assertThat(throwable)
			.isInstanceOf(TaskDoesNotExist.class);
	}

	@Test
	void new_task_is_not_completed() {
		// given list with one new task
		TasksList tasksList = new TasksList(ListId.generate(), "list");
		tasksList.createTask("task");

		// expect that new task is not completed
		assertThat(tasksList.getTasks().get(0).isCompleted()).isFalse();
	}

	@Test
	void task_changes_status() {
		// given list with one new task
		TasksList tasksList = new TasksList(ListId.generate(), "list");
		TaskId taskId = tasksList.createTask("task");

		// when we complete task
		tasksList.changeTaskStatus(taskId, true);

		// then task is completed
		assertThat(tasksList.getTasks().get(0).isCompleted()).isTrue();
	}
}
