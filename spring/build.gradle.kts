import Dependencies.junit
import Dependencies.lombok
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
	java
	id("org.springframework.boot") version "2.1.3.RELEASE"
}

repositories {
	jcenter()
}

apply(plugin = "io.spring.dependency-management")

dependencies {
	implementation(project(":domain"))

	implementation(lombok)
	annotationProcessor(lombok)

	implementation("org.springframework.boot:spring-boot-starter-web")
	testImplementation("org.springframework.boot:spring-boot-starter-test")

	testImplementation(junit)
}

tasks.withType<Test> {
	useJUnitPlatform()
	testLogging {
		exceptionFormat = TestExceptionFormat.FULL
		showStackTraces = true
	}
}
