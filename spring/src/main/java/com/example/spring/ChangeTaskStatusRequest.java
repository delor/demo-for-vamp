package com.example.spring;

import lombok.Data;

@Data
class ChangeTaskStatusRequest {
	private boolean complete;
}
