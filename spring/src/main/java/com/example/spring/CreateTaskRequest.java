package com.example.spring;

import lombok.Data;

@Data
class CreateTaskRequest {
	private String name;
}
