package com.example.spring;

import lombok.Data;

@Data
class CreateTasksListRequest {
	private String name;
}
