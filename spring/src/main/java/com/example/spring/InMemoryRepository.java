package com.example.spring;

import com.example.domain.ListId;
import com.example.domain.TasksList;
import com.example.domain.TasksListRepository;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
class InMemoryRepository implements TasksListRepository {

	private final Map<ListId, TasksList> tasksLists = new HashMap<>();

	@Override
	public List<TasksList> findAll() {
		return Collections.unmodifiableList(List.copyOf(tasksLists.values()));
	}

	@Override
	public void save(TasksList tasksList) {
		tasksLists.put(tasksList.getId(), tasksList);
	}

	@Override
	public Optional<TasksList> get(ListId listId) {
		return Optional.ofNullable(tasksLists.get(listId));
	}
}
