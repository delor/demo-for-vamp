package com.example.spring;

import com.example.domain.TasksListService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
class TaskApplication {
	public static void main(String[] args) {
		SpringApplication.run(TaskApplication.class, args);
	}

	@Bean
	public TasksListService taskListService() {
		return new TasksListService(new InMemoryRepository());
	}
}
