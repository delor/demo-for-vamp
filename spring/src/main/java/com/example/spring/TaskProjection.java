package com.example.spring;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
class TaskProjection {
	private String id;
	private String name;
	private boolean completed;
}
