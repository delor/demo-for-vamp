package com.example.spring;

import com.example.domain.ListId;
import com.example.domain.TaskId;
import com.example.domain.TasksListRepository;
import com.example.domain.TasksListService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
public class TasksController {
	private final TasksListService tasksListService;
	private final TasksListRepository tasksListRepository;

	@PostMapping("/tasks")
	public ResponseEntity createList(
		@RequestBody CreateTasksListRequest request
	) {
		ListId listId = tasksListService.createList(request.getName());

		URI uri = UriComponentsBuilder.newInstance()
			.path("/tasks/{listId}")
			.buildAndExpand(listId)
			.toUri();

		return ResponseEntity.created(uri).build();
	}

	@GetMapping("/tasks")
	public List<TasksListResponse> getLists() {
		return tasksListRepository.findAll().stream()
			.map(TasksListResponse::from)
			.collect(toList());
	}

	@PostMapping("/tasks/{listId}")
	public ResponseEntity createTask(
		@PathVariable String listId,
		@RequestBody CreateTaskRequest request
	) {
		TaskId taskId = tasksListService.createTask(ListId.of(listId), request.getName());

		URI uri = UriComponentsBuilder.newInstance()
			.path("/tasks/{listId}/{taskId}").buildAndExpand(listId, taskId.asString()).toUri();

		return ResponseEntity.created(uri).build();
	}

	@GetMapping("/tasks/{listId}")
	public ResponseEntity getList(
		@PathVariable String listId
	) {
		return tasksListRepository.get(ListId.of(listId))
			.map(TasksListProjection::from)
			.map(ResponseEntity::ok)
			.orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PatchMapping("/tasks/{listId}/{taskId}")
	public ResponseEntity changeTaskStatus(
		@PathVariable String listId,
		@PathVariable String taskId,
		@RequestBody ChangeTaskStatusRequest request
	) {
		tasksListService.changeTaskStatus(ListId.of(listId), TaskId.of(taskId), request.isComplete());
		return ResponseEntity.ok().build();
	}
}
