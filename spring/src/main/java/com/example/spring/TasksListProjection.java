package com.example.spring;

import com.example.domain.TasksList;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
class TasksListProjection {

	private String id;
	private String name;
	@Singular
	private List<TaskProjection> tasks;

	public static TasksListProjection from(TasksList tasksList) {
		TasksListProjectionBuilder builder = TasksListProjection.builder()
			.id(tasksList.getId().asString())
			.name(tasksList.getName());
		tasksList.getTasks().forEach(task -> builder.task(TaskProjection.builder()
			.id(task.getId().asString())
			.name(task.getName())
			.completed(task.isCompleted())
			.build()));
		return builder.build();
	}
}
