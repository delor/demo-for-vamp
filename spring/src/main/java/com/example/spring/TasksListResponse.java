package com.example.spring;

import com.example.domain.TasksList;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
class TasksListResponse {

	private String id;
	private String name;

	public static TasksListResponse from(TasksList tasksList) {
		return TasksListResponse.builder()
			.id(tasksList.getId().asString())
			.name(tasksList.getName())
			.build();
	}
}
