package com.example.spring;

import com.example.domain.ListId;
import com.example.domain.TaskId;
import com.example.domain.TasksList;
import com.example.domain.TasksListRepository;
import com.example.domain.TasksListService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TasksController.class)
class TasksControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TasksListService tasksListService;

	@MockBean
	private TasksListRepository tasksListRepository;

	private ListId listId = ListId.generate();
	private TaskId taskId = TaskId.generate();

	@Test
	void create_task_list() throws Exception {
		when(tasksListService.createList("todo")).thenReturn(listId);

		mockMvc.perform(post("/tasks")
			.content("{ \"name\": \"todo\" }")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isCreated())
			.andExpect(header().string("Location", "/tasks/" + listId.asString()));

		verify(tasksListService).createList("todo");
	}

	@Test
	void get_task_lists() throws Exception {
		when(tasksListRepository.findAll())
			.thenReturn(List.of(new TasksList(ListId.generate(), "todo")));

		mockMvc.perform(get("/tasks"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].name", is("todo")));
	}

	@Test
	void add_task_to_list() throws Exception {
		when(tasksListService.createTask(listId, "task 1")).thenReturn(taskId);

		mockMvc.perform(
			post("/tasks/{listId}", listId.asString())
				.content("{ \"name\": \"task 1\"}")
				.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isCreated());
	}

	@Test
	void finish_task() throws Exception {
		mockMvc.perform(
			patch("/tasks/{listId}/{taskId}", listId.asString(), taskId.asString())
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"complete\": true }"))
			.andExpect(status().isOk());

		verify(tasksListService).changeTaskStatus(listId, taskId, true);
	}

	@Test
	void get_task() throws Exception {
		TasksList tasksList = new TasksList(listId, "todo");
		TaskId taskId1 = tasksList.createTask("first task");
		TaskId taskId2 = tasksList.createTask("second task");
		tasksList.changeTaskStatus(taskId1, true);
		when(tasksListRepository.get(listId))
			.thenReturn(Optional.of(tasksList));

		mockMvc.perform(get("/tasks/{listId}", listId))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id", is(listId.asString())))
			.andExpect(jsonPath("$.name", is("todo")))
			.andExpect(jsonPath("$.tasks[0].id", is(taskId1.asString())))
			.andExpect(jsonPath("$.tasks[0].name", is("first task")))
			.andExpect(jsonPath("$.tasks[0].completed", Matchers.is(true)))
			.andExpect(jsonPath("$.tasks[1].id", is(taskId2.asString())))
			.andExpect(jsonPath("$.tasks[1].name", is("second task")))
			.andExpect(jsonPath("$.tasks[1].completed", Matchers.is(false)))
		;
	}
}
